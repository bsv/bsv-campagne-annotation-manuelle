# BSV Campagne Annotation Manuelle



## Ressources

- Consignes d'annotation
    - [Guide complet](https://docs.google.com/document/d/1zxE0hTj4cChBJaz--2OGIcOHqyf1dMs0/edit)
    - [Présentation (consignes principales)](https://docs.google.com/presentation/d/1Zbo5XF9KN3gTi53573HKD7MiDqK057NMXQvLdJlLMYI/edit?usp=sharing)
- Référentiels pour la normalisation:
    - Organismes Nuisibles et Vecteurs
        - [Liste des organismes nuisibles et vecteurs de la vigne (utilisé pour la pré-annotation)](https://docs.google.com/spreadsheets/d/19bUEFXCfgYm7Pi42yMeOA4ADX9i-SVWzIPBgS6MWHP4/edit?usp=sharing) ⚠ il est conseillé de vérifier que les noms vernaculaires en particulier pointent bien vers le bon taxon. Cette liste reprend en fait les identifiants des deux ressources ci-dessous :
            - [Taxonomie du NCBI](https://www.ncbi.nlm.nih.gov/taxonomy)
            - [Taxref](https://inpn.mnhn.fr/accueil/index)
    - [Maladies de la vigne](https://docs.google.com/spreadsheets/d/1jmtPnJ4pqxUHuv8Yc1tH4YmPQ_maBSbcoUmiQWNVVrI/edit?usp=sharing)
    - [Lieux : Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page)
    - [Cépages : Base de données des collections RFCV (hébergée sur VitiOeno)](https://vitioeno.mistea.inrae.fr/resource/app/germplasm?rdf_type=vocabulary%3AVariety
)
    - Stades phénologiques : Représentation graphique des échelles présentes dans PPDO avec leurs stades
        - [PDF dormance à 11-12 feuilles étalées](https://gitlab.irstea.fr/copain/phenologicalstages/-/blob/master/grapevine/grapevineScalesMappings_1.pdf)
        - [PDF grappe visible à repos végétatif](https://gitlab.irstea.fr/copain/phenologicalstages/-/blob/master/grapevine/grapevineScalesMappings_2.pdf)
    - [Cultures : FrenchCropUsage](https://agroportal.lirmm.fr/ontologies/CROPUSAGE/?p=classes)
- [Forum pour soumettre une question](https://forgemia.inra.fr/bsv/bsv-campagne-annotation-manuelle/-/issues)

## Interface d'annotation

- [Accès AlvisAE](https://bibliome.migale.inrae.fr/bsvreader/alvisae/)
- [Guide utilisateur AlvisAE (en anglais)](https://migale.jouy.inra.fr/redmine/attachments/download/1493/AlvisAE_UI_UserGuide.pdf)

